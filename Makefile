build:
	@docker build -t mikalai-yurkin-allot:latest .

run:
	@docker run -p 8080:8080 mikalai-yurkin-allot:latest

test:
	@docker run mikalai-yurkin-allot pytest --cov=.
