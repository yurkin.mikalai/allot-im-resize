import io
import logging
import typing

from PIL import Image

from image_processing.constants import IMAGE_DEFAULT_NEW_SIZE

log = logging.getLogger(__name__)


class ImageResizeException(Exception):
    pass


class ImageResizer(object):
    @classmethod
    def get_new_image_size(cls):
        return IMAGE_DEFAULT_NEW_SIZE

    @classmethod
    async def resize_image(cls, image_file: typing.BinaryIO) -> io.BytesIO():
        """
        Resize the file-like image object and return the new file-like object 
        This method ignores the aspect ratio and creating the new square image with the given border sizes
        """
        new_size = cls.get_new_image_size()

        try:
            pil_image_object = Image.open(image_file)
            pil_resized_image_object = pil_image_object.resize(
                (new_size, new_size), Image.ANTIALIAS
            )
        except:
            log.exception("Failed to resize the image")
            raise ImageResizeException

        resized_image_file = io.BytesIO()
        pil_resized_image_object.save(
            resized_image_file, format=pil_image_object.format
        )
        resized_image_file.seek(0)
        return resized_image_file
