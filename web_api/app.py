from aiohttp import web

from config import CLIENT_MAX_FILE_SIZE
from web_api import views


def register_routes(_app: web.Application) -> None:
    _app.router.add_route("POST", "/api/image/resize", views.ImageResize)


def app_factory() -> web.Application:
    _app = web.Application(client_max_size=CLIENT_MAX_FILE_SIZE)
    register_routes(_app)
    return _app


app = app_factory()


if __name__ == "__main__":
    web.run_app(app, host="0.0.0.0", port=8080)
