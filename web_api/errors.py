API_IMAGE_RESIZE_INVALID_PAYLOAD_FORMAT = "Invalid payload format. Pass the image as the `image` using the multipart-form encoding"
API_IMAGE_RESIZE_INVALID_FILE = "Invalid image file. Cannot resize"
