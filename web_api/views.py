from aiohttp import web
from multidict import MultiDict

from image_processing.resize import ImageResizeException, ImageResizer
from web_api import errors


class ImageResize(web.View):
    async def post(self) -> web.Response:

        try:
            # NOTE: we are going to load the whole file in to the memory for the simplicity with the assumption
            # that we are not dealing with the big files here; big files have to be rejected before being passed
            # to the app
            payload = await self.request.post()
            image_file = payload["image"].file
            image_name = payload["image"].filename
            resized_image = await ImageResizer.resize_image(image_file)

        except (KeyError, AttributeError):
            return web.Response(
                text=errors.API_IMAGE_RESIZE_INVALID_PAYLOAD_FORMAT, status=400
            )
        except ImageResizeException:
            return web.Response(text=errors.API_IMAGE_RESIZE_INVALID_FILE, status=400)

        return web.Response(
            body=resized_image.read(),
            headers=MultiDict(
                {"Content-Disposition": f'attachment; filename="resized_{image_name}"'}
            ),
        )
