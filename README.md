# allot-im-resize

#### Usage

   * To build the image run `make build`
   * To run the tests inside the image run `make test`
   * To run the web app run the `make run`

#### Implementation details

The app is a [aiohttp](https://aiohttp.readthedocs.io/en/stable/) based web application. 
The image resizing logic realized via in-app transformation using [Pillow](https://pillow.readthedocs.io/en/stable/). 
The tests are based on the [pytest](https://pytest.org/en/latest/) framework.

#### Implementation notes

The scaling of the app has to be performed by some external web services / balancers. Its details are out of the scope of the test task. 
Also, it is the responsibility of these web services / balancers to limit the size of the image to be passed to the application. 
BTW the application has the config used to control the max available size of client data passed to the app (you may control it bypassing the env variable `CLIENT_MAX_FILE_SIZE`, in bytes, 
the default is `4194304` = 4 MB).

The goal was to show some hands-on experience with the python3, which explains the decision to choose the web framework based on asyncio which is part of python3. 
Also, for the same reason, I've decided to use in-app image processing with the python library instead of calling external services. The app does not store in DB the incoming original and resized 
images, because this is not the requirement of the test task. The app does not log errors / exception using some external services like Sentry, because this is not the requirement of the test task.
 
As a result the CPU-bound image processing executed inside the app removes the benefits of using the async IO mechanism as there are not so many IO operations.
For the production usage, the solution has to be altered to use the specialized external service available via the HTTP calls (like kraken.io, imgix.com, etc). 
In this case, we will utilize the benefits of non-blocking IO operations and delegate the CPU-bound to resize operations to the external services.

Another suitable approach for the production is to quickly store the images and then delegate the image processing to the separate background workers. But this approach if going far from the simple 
test task as require more complex infrastructure. 
    