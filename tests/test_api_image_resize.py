from io import BytesIO

import pytest
from PIL import Image
from aiohttp import FormData

from config import CLIENT_MAX_FILE_SIZE
from tests.utils import SyntheticImageFactory


class TestApiImageResize(object):
    @staticmethod
    def resize_the_image(image_file: BytesIO, new_size: int):
        pil_image_object = Image.open(image_file)
        pil_resized_image_object = pil_image_object.resize(
            (new_size, new_size), Image.ANTIALIAS
        )
        resized_image_file = BytesIO()
        pil_resized_image_object.save(
            resized_image_file, format=pil_image_object.format
        )
        resized_image_file.seek(0)
        return resized_image_file

    @staticmethod
    def build_image_multipart_payload(
        image_file: BytesIO, image_format: str
    ) -> FormData:
        """
        Build the multipart form payload with the image data
        :return: 
        """
        form = FormData()
        form.add_field(name="image", filename=f"image.{image_format}", value=image_file)
        return form

    @staticmethod
    def build_broken_multipart_payload(data: bytes) -> FormData:
        """
        Build the multipart form payload with the file that just imtitates the image but not the actually image
        :return: 
        """
        bad_content = BytesIO(data)
        bad_content.seek(0)
        form = FormData()
        form.add_field(name="image", filename="image.png", value=bad_content)
        return form

    async def test_post_no_body(self, client):
        response = await client.post("/api/image/resize")
        assert response.status == 400
        assert (
            await response.content.read()
            == b"Invalid payload format. Pass the image as the `image` using the multipart-form encoding"
        )

    async def test_post_wrong_body_payload_json(self, client):
        response = await client.post("/api/image/resize", json={"image": ""})
        assert response.status == 400
        assert (
            await response.content.read()
            == b"Invalid payload format. Pass the image as the `image` using the multipart-form encoding"
        )

    async def test_post_wrong_malformed_image(self, client):
        malformed_payload = self.build_broken_multipart_payload(
            b"I am definitely not an image content"
        )
        response = await client.post("/api/image/resize", data=malformed_payload)
        assert response.status == 400
        assert await response.content.read() == b"Invalid image file. Cannot resize"

    async def test_post_too_big_payload(self, client):

        # generate synthetic payload with the size in bytes > given limit
        size_limit = CLIENT_MAX_FILE_SIZE
        size_over_limit = size_limit + 1
        huge_payload = b"X" * size_over_limit

        malformed_huge_payload = self.build_broken_multipart_payload(huge_payload)
        response = await client.post("/api/image/resize", data=malformed_huge_payload)
        assert response.status == 413
        assert await response.content.read() == bytes(
            f"Maximum request body size {size_limit} exceeded, actual body size {size_over_limit}",
            encoding="utf-8",
        )

    @pytest.mark.parametrize("image_format", ["jpeg", "png", "bmp", "gif"])
    async def test_post_good_body(self, client, image_format):

        initial_size = 50
        expected_size = 224

        image_factory = SyntheticImageFactory(initial_size, image_format)

        initial_image_file = image_factory()
        expected_resized_image_file = self.resize_the_image(
            image_factory(), expected_size
        )

        response = await client.post(
            "/api/image/resize",
            data=self.build_image_multipart_payload(initial_image_file, image_format),
        )
        assert response.status == 200
        assert await response.content.read() == expected_resized_image_file.getvalue()
