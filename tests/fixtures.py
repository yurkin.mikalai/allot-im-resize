import pytest

from web_api.app import app_factory


@pytest.fixture
def client(loop, aiohttp_client):
    """
    Simple HTTP client fixture for the API tests
    """
    return loop.run_until_complete(aiohttp_client(app_factory()))
