from io import BytesIO

from PIL import Image


class SyntheticImageFactory(object):
    """
    Use to generate the synthetic simple single-color image file-like objects
    """

    def __init__(self, image_size: int, image_format: str, color: str = "green"):
        self.image_size = image_size
        self.image_format = image_format
        self.color = color

    def __call__(self, *args, **kwargs):
        synth_image = Image.new("RGB", (self.image_size, self.image_size), self.color)
        image_file = BytesIO()
        synth_image.save(image_file, format=self.image_format)
        image_file.seek(0)
        return image_file
