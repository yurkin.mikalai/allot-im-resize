FROM python:3.6.9
RUN mkdir /backend
WORKDIR /backend
COPY . /backend/
RUN pip install -r requirements.txt
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "--worker-class", "aiohttp.GunicornWebWorker", "web_api.app:app"]
