from decouple import config


# set the default max size to be pretty big, because generally we want to
# limit the size on the web server / balancer side
CLIENT_MAX_FILE_SIZE = config("CLIENT_MAX_FILE_SIZE", 4194304, cast=int)  # 4 MB
